import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class Player extends Element implements KeyListener {

protected int velX = 0;
private int velY = 0;
private boolean ps=false;
public boolean hitleft,hitright;

protected int x,y,radius;
Laser las;
Laser las2;

public Laser getLeftLaser(){
	return las;
}

public Laser getRightLaser(){
	return las2;
}

 public Player(){ 
	 hitleft=false;
	 hitright=false;	 
 }

public Player(int xx,int yy, int rad)
{
	x=xx;
	y=yy;
	radius = rad;
	las = new Laser(x-6,y+6);
	las2 = new Laser(x+15,y+6);
}

public void paint (Graphics g)
{
	g.fillOval(x, y, radius,radius);
	las.paint(g);
	las2.paint(g);
}


	public void update(MainClass mc)
	{
		mc.addKeyListener(this);
		x+=velX;
		y+=velY;	
		
		if(!ps)
		{
			las.x+=velX;
			las2.x+=velX;
			las.y+=velY;
			las2.y+=velY;
		}
		
		if(ps)
		{
			if(ps)
			{
				las.update(mc);
				las2.update(mc);
			}
			
			/*if(hitleft)
			las.update(mc);
			
			if(hitright)
			las2.update(mc);*/
			
			if(hitleft)
			{
				hitleft=false;
				las.y = y+6;
				las.x = x-6;
			}
			if(hitright)
			{
				hitright=false;
				las2.y = y+6;
				las2.x = x+15;
			}
			
			if(las.y<=-80)
			{
				ps=false;
				las.y = y+6;
				las.x = x-6;
			}
			
			if(las2.y<=-80)
			{
				ps=false;
				las2.y = y+6;
				las2.x = x+15;
			}
			
			
		}
		
	}

		@Override
		public void keyPressed(KeyEvent e) {
				switch(e.getKeyCode()){
						case KeyEvent.VK_RIGHT:{
							velX=5;
							break;
						
						}
						case KeyEvent.VK_LEFT:{
							velX=-5;
							break;
						}
						
						case KeyEvent.VK_UP:{
							velY=-5;
							break;
						}
						
						case KeyEvent.VK_DOWN:{
							velY=5;
							break;
						}
						case KeyEvent.VK_SPACE:{
							ps=true;
							hitleft=true;
							hitright=true;
							break;
						}
						
				}
	
		}

		@Override
		public void keyReleased(KeyEvent e) {
		
			switch(e.getKeyCode()){
					case KeyEvent.VK_RIGHT:{
						velX=0;
						break;
					
					}
					case KeyEvent.VK_LEFT:{
						velX=0;
						break;
					}
					
					case KeyEvent.VK_UP:{
						velY=0;
						break;
					}
					
					case KeyEvent.VK_DOWN:{
						velY=0;
						break;
					}
			
			}
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		
		public int getx()
		{
			return x;
		}
		public int gety()
		{
			return y;
		}
		
		
		
		

	
}
