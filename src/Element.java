import java.awt.Graphics;


public abstract class Element   {
	
    int x,y,radius,Speed;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}


	String whatIs;
	
	public void elemento(int cx, int cy, int crad, int cspeed, String figure)
	{
		System.out.println("working fine");
		x=cx;
		y=cy;
		radius = crad;
		Speed = cspeed;
		whatIs = figure;
	}
	
	public void paint(Graphics g)
	{
		
		switch(whatIs)
		{
			case ("circle"):
			{
				g.fillOval(x, y, radius, radius);
				break;
			}
			case ("rectangle"):
			{
				g.fillRect(x, y, radius, radius);
				break;
			}
		}
	}

	
	//update is different for most classes = abstract here
	public abstract void update(MainClass mc);
	
	


}
