import java.applet.*;
import java.awt.*;
import java.util.ArrayList;

public class MainClass extends Applet implements Runnable {

	private static final long serialVersionUID = 1L;
	Thread thread = new Thread(this);
	boolean running = true;
	Image dbImage;
	Graphics dbg;
	Player p;
	Enemy e = new Enemy();
	Enemy e1 = new Enemy();
	
	ArrayList <Element> mother = new ArrayList<Element>();
	
	public void init(){
		setSize(400,400);
	
		p  = new Player(100,200,20);

		mother.add(e);
		mother.add(e1);
		mother.get(0).elemento(100+3*50, 10, 30, 5, "rectangle");//enemy
		mother.get(1).elemento(100+100, 20, 40, 25, "circle");//enemy


	};
	
	public void start(){thread.start();}
	public void destroy(){running = false;};
	public void stop(){running = false;}
	public void run() 
	{
		while(running)
		{
			repaint();
			for(Element z:mother)
			z.update(this);//enemy
			p.update(this);//player
			collision();
			
				try
				{
					Thread.sleep(20);
				}
				catch(InterruptedException e)
				{
					System.out.println("Error has occured");
				}
		}
	}//run end
	
	public void update(Graphics g)
	{
		dbImage = createImage(400,400);
		dbg = dbImage.getGraphics();
		paint(dbg);
		g.drawImage(dbImage,0, 0, this);
	}
	
	public void paint(Graphics g)
	{
		for(Element z:mother)
		z.paint(g);
		p.paint(g);
	}
	
	public void collision()
	{
		int lx = p.getLeftLaser().x;
		int ly = p.getLeftLaser().y;
	
		int px = p.getRightLaser().x;
		int py = p.getRightLaser().y;
		
		int EnemyA_x = mother.get(0).x;
		int EnemyA_y = mother.get(0).y;
		
		int EnemyB_x = mother.get(1).x;
		int EnemyB_y = mother.get(1).y;
		p.hitleft=false;
		p.hitright=false;
		if(((lx >= EnemyA_x && lx <= EnemyA_x+30)&&(ly <= EnemyA_y+30 && ly >= EnemyA_y)) || ((lx >= EnemyB_x && lx <= EnemyB_x+40)&&
				(ly <= EnemyB_y+40 && ly >= EnemyB_y)))
		{
			System.out.println("Bang!->Left Shot");
			p.hitleft=true;
		}
		
		if(((px >= EnemyA_x && px <= EnemyA_x+30)&&(py <= EnemyA_y+30 && py >= EnemyA_y)) || ((px >= EnemyB_x && px <= EnemyB_x+40)&&
				(py <= EnemyB_y+40 && py >= EnemyB_y)))
		{
			System.out.println("Bang!->Right shot");
			p.hitright=true;
		}
		
	}
	
}